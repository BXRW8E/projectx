package hf.chat.UI;

import hf.chat.MainChat;
import hf.chat.model.filetransfer.FileClient;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;

import java.io.File;
import java.net.URL;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ResourceBundle;



/**
 * Created by mate on 2016.11.22..
 */
public class RootLayoutController implements Initializable {

    @FXML
    public TextArea messages;
    @FXML
    public Label myName;
    @FXML
    Button sendButton;
    @FXML
    Button fileSendButton;
    @FXML
    TextField msgToSend;
    //Stage stage = new Stage();
    // Reference to the main application
    private MainChat mainChat;

    /**
     * Saját magára mutató referencia
     *
     * @param mainChat
     */
    public void setMainApp(MainChat mainChat) {
        this.mainChat = mainChat;
    }

    /**
     * A Send gomb megnyomására elküldi az üzenetet a másik félnek
     * Ha nulla hosszú az üzenet nem csinál semmit
     */
    @FXML
    private void handleSendButton() {
        StringBuilder message = new StringBuilder();
        Timestamp time = new Timestamp(System.currentTimeMillis());
        message.append( "[" + mainChat.getSettings().getNickName() + " #");
        message.append(time.toLocalDateTime().toString().substring(11,19) +"]: ");
        message.append( msgToSend.getText());
        if (msgToSend.getText().length() > 0) {
            msgToSend.clear();
            //  messages.appendText(mainChat.getSettings().getNickName());
            messages.appendText(message.toString() + "\n");
            try {
                // mainChat.connection.send();
                mainChat.connection.send(message.toString());
            } catch (Exception e) {
                messages.appendText(">> Error by sending: " + message.toString() + "\n");
                e.printStackTrace();
            }
        }
    }

    /**
     * Meghívja a handleSendButton metódust ha ENTER nyomódik
     *
     * @param event az Enter lenyomása
     */
    @FXML
    private void handleSendOnEnter(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) handleSendButton();
    }

    /**
     * egy FileChooser segítségével ki tudjuk választani a küldendő fájlt
     */
    @FXML
    private void handleFileSend() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.setInitialDirectory(new File("/home/"));  //deafault library
        File file = fileChooser.showOpenDialog(null);
        if (file != null) {
            messages.appendText(file.getAbsolutePath() + "\n");
            String absolutPath = file.getAbsolutePath();
            System.out.println(file.getAbsolutePath());
            try {
                if(mainChat.connection.isServer())
                    mainChat.fileClient = new FileClient(mainChat.getSettings().getServerIp(), mainChat.getSettings().getPort()+1, absolutPath);
                else mainChat.fileClient = new FileClient(mainChat.getSettings().getServerIp(), mainChat.getSettings().getPort()+2, absolutPath);
            } catch (Exception e) {
                messages.appendText(">> ERROR by file sending" + absolutPath);
            }
        }
    }

    /**
     * Uj chatet nyit meg
     */
   /* @FXML
    private void handleNew() throws IOException {
        MainChat newChat = new MainChat();
        newChat.start(newChat.getPrimaryStage());
    }*/

    /**
     * A quit gomb hatására bezárjódik az alkalmazás
     */
    @FXML
    private void handleQuit() {
        try {
            //mainChat.connection.closeConnection();
            mainChat.stop();
        } catch (Exception e) {
            e.printStackTrace();
            hf.chat.util.Alert alert = new hf.chat.util.Alert("QuitError", "Error by quit");
        } finally {
            System.exit(0);
        }
    }

    /**
     * About ablak
     */
    @FXML
    private void handleAbout() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("FxChat");
        alert.setHeaderText("About");
        alert.setContentText("Author: Bártfai Máté\n Version: " + MainChat.version + "\n Source: " +
                "https://bitbucket.org/BXRW8E/projectx");

        alert.showAndWait();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // System.out.println("Loading chat...");
        //messages.textProperty().bindBidirectional(mainChat.history);
    }
}

