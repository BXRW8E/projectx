package hf.chat.UI;

import hf.chat.util.Alert;
import hf.chat.util.ConnectionSettings;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by mate on 2016.11.22..
 */
public class ConnectionSettingsController {

    boolean connectClicked = false;
    ConnectionSettings settings = null;
    Properties properties;
    @FXML
    private TextField serverIPAddress;
    @FXML
    private TextField portAddress;
    @FXML
    private RadioButton isServerBox;
    @FXML
    private RadioButton isClientBox;
    @FXML
    private Button connectButton;
    @FXML
    private Button closeButton;
    @FXML
    private Button chooseFolder;
    @FXML
    private TextField nickName;
    private Stage dialogStage;

    public ConnectionSettings settings() {
        return this.settings;
    }

    @FXML
    private void initialize() {
    }


    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * ha a Connect buttonra klikkel a felhasználó
     * a beállításoknak megfelelően töltődik be a chat
     * ha valami hiányzik, egy figyelmeztető ablak szól hogy töltsük ki
     */
    @FXML
    private void handleConnectButton() {
        settings = new ConnectionSettings();
        Stage stage = (Stage) connectButton.getScene().getWindow();
        String serverIPtext = "127.0.0.1";
        try {
            settings.setPort(Integer.parseInt(portAddress.getText()));
            settings.setNickName(nickName.getText());
            if (isClientBox.isSelected()) {
                settings.setServerIp(serverIPAddress.getText());
                settings.setServer(false);
                //isClient = true;
                System.out.println(settings.getServerIp());
            } else if (isServerBox.isSelected()) {
                settings.setServer(true);
            }

            if (portAddress.getText().length() < 1 || Integer.parseInt(portAddress.getText()) < 0 ||
                    nickName.getText().length() < 1) throw new Exception("Check the fields");
            else {
                connectClicked = true;
                stage.close();
            }
        } catch (Exception e) {
            Alert alert = new Alert(e.getMessage(), "Please check the fields. ");
            connectClicked = false;
        }

    }
    @Deprecated
    public void loadProperties() {

        try {
            properties.load(new FileInputStream("location.properties"));
        } catch (IOException e) {
            Alert alert = new Alert("File Not Foun", "Properties file not found");
        }
    }
    @Deprecated
    public void saveProperties() {
        try {
            FileOutputStream s = new FileOutputStream("location.properties");
            properties.store(s, "Program properties");
        } catch (IOException e) {
            Alert alert = new Alert("File Not Foun", "Properties file not found");
        }
    }

    /**
     * Segítségével kiválaszthatjuk, hogy mi legyen az alapértelmezett könyvtár ahova menthetünk
     */
    @FXML
    private void handleFileChooser() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Open Resource Folder");
        File file = directoryChooser.showDialog(null);

        if (file != null && file.isDirectory()) {
            String folder = file.getAbsolutePath();
            String propName = "location.properties";
            Properties locationProp = new Properties();

            File prop = new File(propName);

            /*
            ha létezik ilyen nevű properti megnyitjuk és átírjuk a saveDir propot
            ha nincs akkor csinálunk egyet és elmentjük a saveDir-be a path-t
             */
            if (prop.exists() && !prop.isDirectory()) {
                try {
                    locationProp.load(new FileInputStream(propName));
                    locationProp.setProperty("saveDir", folder);
                    locationProp.store(new FileOutputStream(propName), "");
                } catch (IOException e) {

                }
            } else {
                try {
                    locationProp.setProperty("saveDir", folder);
                    locationProp.store(new FileOutputStream(propName), "");
                } catch (IOException e) {
                    System.out.println("Cant store properties");
                }
            }

        }
    }

    /**
     * Close gomb hatására default értékeket állít be az alkalmazás
     * és olyan mintha megynomta volna a connect gombot a felhazsnáló.
     */
    @FXML
    private void handleCloseButton() {
        //  dialogStage.close();
        settings = new ConnectionSettings();
        settings.setNickName("John Doe");
        settings.setPort(1234);
        settings.setServer(true);
        settings.setServerIp("127.0.0.1");
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
        connectClicked = true;
    }


    public boolean isConnectClicked() {
        return connectClicked;
    }

    /**
     * Beállítja a serverIPAdress textfield ipcímet
     *
     * @param serverIP szerver IP címe
     */
    public void setServerIPAddress(String serverIP) {
        if (serverIP.length() > 0) {
            this.serverIPAddress.setText(serverIP);
        }
    }

    /**
     * Beállítja a portAddress értékét
     *
     * @param port a port címe
     */
    public void setPortAddress(int port) {
        if (port > 0) {
            this.portAddress.setText(Integer.toString(port));
        }
    }

}
