package hf.chat;

import hf.chat.UI.ConnectionSettingsController;
import hf.chat.UI.RootLayoutController;
import hf.chat.model.Client;
import hf.chat.model.NetworkConnection;
import hf.chat.model.Server;
import hf.chat.model.filetransfer.FileClient;
import hf.chat.model.filetransfer.FileServer;
import hf.chat.util.Alert;
import hf.chat.util.ConnectionSettings;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by mate on 2016.11.22..
 */
public class MainChat extends Application implements Initializable {

    public static final double version = 1.0;
    public NetworkConnection connection = null;
    private FileServer fileServer = null;
    private RootLayoutController rootLayoutController;
   // private StringProperty sent = new SimpleStringProperty();
    private Stage primaryStage;
    private ConnectionSettings settings = new ConnectionSettings();
    public FileClient fileClient = null;


    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Ez az alkalmazás fő vezérlője
     * Kezeli a csevegőablakot
     * Több székon fut a program, hogy ne blokkolják egymást a folyaatok
     * @see NetworkConnection
     * @param primaryStage
     * @throws IOException
     */
    @Override
    public void start(Stage primaryStage) throws IOException {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("FxChat");

        showConnectionSettings();       //megnyitja a connection settings ablakot
        initRootLayout();  //betölti a fő keretet
        System.out.println("Main: " + settings.getPort());
        System.out.println("Server: " + ConnectionSettings.isServer());

        if (ConnectionSettings.isServer()) {
            connection = createServer(settings.getPort()); //létrehoz egy szervert vagy klienst, connection egy szálon fut
            fileServer = new FileServer(settings.getPort() + 2);
           //ThreadServer = new Thread(()->{ fileServer = new FileServer(settings.getPort()+1);});
            //fileClient = new FileClient(settings.getPort() + 1, ConnectionSettings.getServerIp());
        } else {
            connection = createClient(settings.getPort(), ConnectionSettings.getServerIp());
            fileServer = new FileServer(settings.getPort() + 1);
            //ThreadServer = new Thread(()->{ fileServer = new FileServer(settings.getPort()+2);});
            //fileClient = new FileClient(settings.getPort() + 2, ConnectionSettings.getServerIp());
        }

        try {
            connection.startConnection();

            //FileClient receiver = new FileClient(settings.getPort()+1, settings.getServerIp());
            //System.out.println(fileClient.sent.getValue());
            /*fileClient.sent.addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    if (fileClient.sent.getValue() == "#FILE#") {

                        fileServer.start(); //fogadás

                        messages.delete(0, rootLayoutController.messages.getText().length());
                        messages.append(rootLayoutController.messages.getText());
                        messages.append(">>: Sending file...\n");
                        history.set(messages.toString());

                    } else if (fileClient.sent.getValue() == "#ENDFILE#") {
                        //ThreadClient.stop();
                        messages.delete(0, rootLayoutController.messages.getText().length());
                        messages.append(rootLayoutController.messages.getText());
                        messages.append(">>: File sent succesful\n");
                        history.set(messages.toString());
                    }
                    System.out.println(fileClient.sent.getValue());
                }
            });
            */


            rootLayoutController.messages.appendText(">>: Started at PORT: "+settings.getPort()+"\n");
           // System.out.println("history: " + history.getValue());
            //System.out.println(messages);

        } catch (Exception e) {
            e.printStackTrace();
            // System.out.println("Error by startConnection");
            Alert alert = new Alert("Error by startConnection");
        }
    }

    /**
     * Inicializáló függvény. Start() előtt hívódik meg.
     * @throws Exception
     */
    @Override
    public void init() throws Exception {
          System.out.println("Main starting...");
    }

    /**
     * Bezárákor hívódik meg
     * @throws Exception Kivételt dob ha nincs lezárva valamelyik socket
     */
    @Override
    public void stop() throws Exception {
        connection.closeConnection();
        fileClient.close();
        fileServer.close();

    }

    /**
     * Keret betöltése UI/RootLayout.fxml fájlból
     * Ez a főablak. Itt van a menü és a csevegő rész is
     */
    private void initRootLayout() {
        try {
            // Betölti a rootlayoutot az FXML fájlból
            FXMLLoader loaderRoot = new FXMLLoader();
            loaderRoot.setLocation(MainChat.class.getResource("UI/RootLayout.fxml"));  // ez a keret
            BorderPane rootLayout = loaderRoot.load();
            // RootLayoutController rootLayoutController = loaderRoot.getController(); //referencia
            rootLayoutController = loaderRoot.getController();
            rootLayoutController.setMainApp(this);

            rootLayoutController.myName.setText("I am " + ConnectionSettings.getNickName());

            //history.set("ez már új\n");
            //rootLayoutController.messages.setText(history.toString());   // ez valami iylen lenne jó
            //A jelenet a rootlayoutban

            Scene sceneRoot = new Scene(rootLayout);
            primaryStage.setScene(sceneRoot);
            primaryStage.show();


        } catch (IOException e) {
            //e.printStackTrace();
            //System.out.println("Error with the initRootLayout() ");
            Alert alert = new Alert("Error with the initRootLAyout");
        }
    }

    /**
     * Létrehoz egy servert egy külön szálon
     * Bejövő üzeneteket összefűzi
     *
     * @param port ahova csatlakozni szeretnénj
     * @return egy Servert
     */
    private Server createServer(int port) {
        return new Server(port, (Serializable data) -> {
            Platform.runLater(() -> {
                rootLayoutController.messages.appendText(data.toString()+"\n");
            });
        });
    }

    /**
     * Létrehoz egy klienst egy külön szálon
     * A bejövő üzeneteket hozzáfűzi a messages-hez
     *
     * @param port  ahova csatlakozni szeretnénk
     * @param serveraddress szerverIP címe
     * @return Egy clienst
     */
    private Client createClient(int port, String serveraddress) {
        return new Client(serveraddress, port, (Serializable data) -> {
            Platform.runLater(() -> {
                rootLayoutController.messages.appendText(data.toString()+"\n");
            });
        });

    }


    /**
     * Connection beállítások megnyitása egy új ablakban.
     *
     * @return true ha megnyomták a connect gombot
     */
    private boolean showConnectionSettings() {
        try {
            // Betölti az FXML fájlt és megmutatja.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainChat.class.getResource("UI/ConnectionSettings.fxml"));
            AnchorPane page = loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Connection");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            ConnectionSettingsController controller = loader.getController();
            controller.setServerIPAddress("127.0.0.1"); //default IP
            controller.setPortAddress(5555); //default port


            //Betölti a dialógusablakot és addig vár amíg a felhasználó nem  zárja be vagy connectál
            dialogStage.showAndWait();
            settings = controller.settings();

            return controller.isConnectClicked();
        } catch (IOException e) {
            e.printStackTrace();
            // System.out.println("Error with the showConnectionSettings()");
            Alert alert = new Alert("Error with the showConnectionSettings");
            return false;
        }
    }

    /**
     * Visszaadja a fő Stage-et
     *
     * @return primaryStage
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    /**
     * Az adott futó programra visszaadja a beállításokat.
     *
     * @return this.settings
     */
    public ConnectionSettings getSettings() {
        return this.settings;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("Main starting...");
        // Alert start = new Alert("Loading chat...");
    }

}