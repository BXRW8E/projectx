package hf.chat.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by mate on 2016.11.29..
 */
public class SendThread implements Runnable {
    Socket sock = null;
    PrintWriter print = null;
    BufferedReader brinput = null;

    public SendThread(Socket sock) {
        this.sock = sock;
    }//end constructor

    @Override
    public void run() {
        try {
            if (sock.isConnected()) {
                System.out.println("Client connected to " + sock.getInetAddress() + " on port " + sock.getPort());
                this.print = new PrintWriter(sock.getOutputStream(), true);
                System.out.println("Type your message to send to server..type 'EXIT' to exit");
                while (true) {
                    if (!send()) break;
                }
                sock.close();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }//end run method

    public boolean send() throws IOException {
        boolean sent = true;
        // try{
        // while (true) {
        brinput = new BufferedReader(new InputStreamReader(System.in));
        String msgtoServerString = null;
        msgtoServerString = brinput.readLine();
        this.print.println(msgtoServerString);
        this.print.flush();
        //} catch (IOException e) {
        //  e.printStackTrace();
        //}
        //}//end class
        if (msgtoServerString == "EXIT") sent = false;
        return sent;
    }
}