package hf.chat.test;

/**
 * Created by mate on 2016.12.16..
 */
import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class FileServer extends Thread {

    private ServerSocket ss;
    private String path;

    public FileServer(int port, String path) {
        this.path = path;
        try {
            ss = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        while (true) {
            try {
                Socket clientSock = ss.accept();
                saveFile(clientSock, path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void saveFile(Socket clientSock, String path) throws IOException {
        DataInputStream dis = new DataInputStream(clientSock.getInputStream());
        FileOutputStream fos = new FileOutputStream(path);
        byte[] buffer = new byte[4096];


        int filesize = 0; // Send file size in separate msg
        filesize = Integer.parseInt(dis.readUTF());
        int read = 0;
        int totalRead = 0;
        int remaining = filesize;
        while((read = dis.read(buffer, 0, Math.min(buffer.length, remaining))) > 0) {
            totalRead += read;
            remaining -= read;
            System.out.println("read " + totalRead + " bytes.");
            fos.write(buffer, 0, read);
        }

        fos.close();
        dis.close();
       // ss.close();
        clientSock.close();
        //ss.close();
    }

    public static void main(String[] args) {
        FileServer fs = new FileServer(1988,"/home/mate/Downloads/test/src1.pdf");
        fs.start();
        FileServer fs1 = new FileServer(1989, "/home/mate/Downloads/test/src2.zip");
        fs1.start();
    }

}