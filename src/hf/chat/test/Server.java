package hf.chat.test;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by mate on 2016.11.29..
 */
public class Server {
    public static void main(String[] args) throws IOException {

        final int port = 444;
        // System.out.println("Server waiting for connection on port "+port);
        //  ServerSocket ss = null;
        Thread server = new Thread(() -> {
            try {
                System.out.println("Server waiting for connection on port " + port);
                ServerSocket ss = null;
                ss = new ServerSocket(port);
                Socket clientSocket = ss.accept();
                System.out.println("Recieved connection from " + clientSocket.getInetAddress() + " on port " + clientSocket.getPort());
                //create two threads to send and recieve from client
                RecieveFromClientThread recieve = new RecieveFromClientThread(clientSocket);
                Thread thread = new Thread(recieve);
                thread.start();
                SendToClientThread send = new SendToClientThread(clientSocket);
                Thread thread2 = new Thread(send);
                thread2.start();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Hiba a szerverszálban");
            }

        });


    }

    public void sendMsg(String msg) {

    }
}
