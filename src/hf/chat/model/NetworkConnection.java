package hf.chat.model;

import javafx.beans.property.SimpleStringProperty;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;
import java.util.function.Consumer;

/**
 * Created by mate on 2016.11.19..
 * Abstarct osztály. Ez valósítja meg a hálózati kapcsolatot. Bármilyen sorosítható adatot lehet küldeni.
 * Stringeket, fájlokat stb.
 * <link>www.youtube.com/watch?v=VVUuo9VO2II</link>
 */
public abstract class NetworkConnection {

    public SimpleStringProperty sent = new SimpleStringProperty("false");
    private Consumer<Serializable> onReceiveCallback;
    private ConnectionThread connectionThread = new ConnectionThread();
    private SimpleStringProperty input;
    private Serializable data;

    /**
     * Constructor
     *
     * @param onReceiveCallback
     */
    public NetworkConnection(Consumer<Serializable> onReceiveCallback) {
        this.onReceiveCallback = onReceiveCallback;
        connectionThread.setDaemon(true);
    }

    public NetworkConnection(SimpleStringProperty input) {
        this.input = input;
        connectionThread.setDaemon(true);
    }

    /**
     * Elindít egy szálat a csatlakozáshoz.
     *
     * @throws Exception
     */
    public void startConnection() throws Exception {
        connectionThread.start();
    }

    /**
     * Bezárja a socketet és a szálat is.
     *
     * @throws Exception
     */
    public void closeConnection() throws Exception {
        connectionThread.socket.close();

    }

    /**
     * Bármiylen adat küldhető, amin rajta van a Serializible interface.
     *
     * @param data
     * @throws Exception
     */
    public void send(Serializable data) throws Exception {
        connectionThread.out.writeObject(data);
    }

    /**
     * File-t tudunk másolni
     *
     * @param file
     * @throws IOException
     */
    public void sendFile(File file) throws IOException {
        new Thread(() -> {
            sent.set("false");
            String propName = "location.properties";
            String path = null;
            Properties locationProp = new Properties();
            File prop = new File(propName);

            if (prop.exists() && !prop.isDirectory()) {
                try {
                    locationProp.load(new FileInputStream(propName));
                    path = locationProp.getProperty("saveDir");
                } catch (IOException e) {
                    System.out.println("Prop not found");
                }

            } else path = "/home/mate/";

            File save = new File(path + "/" + file.getName());
            FileOutputStream fout = null;
            int len = 0;
            try {
                System.out.println("File send start...");
                fout = new FileOutputStream(save);
                FileInputStream fin = new FileInputStream(file);
                if (file.length() > Integer.MAX_VALUE) len = (int) file.length() / 2;
                else len = (int) file.length();
                byte[] header = new byte[len];
                fin.read(header);
                fout.write(header);
                System.out.println(file.getClass());
                while (true) {
                    int data = fin.read(header);
                    System.out.println(data);
                    if (data == -1) break;
                    fout.write(data);
                }
                fin.close();
                fout.close();
                sent.set("true");
                System.out.println("File sent...");
                System.out.println(save.getAbsolutePath());
            } catch (FileNotFoundException e) {
                System.out.println("File not found..." + file.getAbsolutePath() + "-->" + save.getAbsolutePath());
            } catch (IOException e1) {
                System.out.println("Error by sending File...");
            }
        }).start();
    }


    public String getData() {
        return this.data.toString();
    }

    public abstract boolean isServer();

    protected abstract String getIP();

    protected abstract int getPort();

    private class ConnectionThread extends Thread {
        private Socket socket;
        private ObjectOutputStream out;

        @Override
        public void run() {
            try (ServerSocket server = isServer() ? new ServerSocket(getPort()) : null;
                 Socket socket = isServer() ? server.accept() : new Socket(getIP(), getPort());
                 ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
                 ObjectInputStream in = new ObjectInputStream(socket.getInputStream())) {

                // System.out.println("Starting...");
                this.socket = socket;
                this.out = out;
                socket.setTcpNoDelay(true);   // gyorsabb üzenetküldés
                while (true) {
                    //Serializable data = (Serializable) in.readObject();
                    data = (Serializable) in.readObject();
                    //System.out.println(data.getClass());
                    onReceiveCallback.accept(data);

                    //input = new SimpleStringProperty(data.toString());
                    //System.out.println("Network conn: " + data);
                }
            } catch (Exception e) {
                onReceiveCallback.accept("Connection closed");
                //input = new SimpleStringProperty("Connection closed");

            }
        }
    } //connectionthread


} //networkconnection
