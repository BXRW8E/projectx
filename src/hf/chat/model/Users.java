package hf.chat.model;

/**
 * Created by mate on 2016.11.22..
 */
public class Users {

    public String userName;
    public boolean isOnline = false;

    /**
     * Getter
     *
     * @return
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Setter
     *
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Ha online van a felhasznalo igazat ad vissza
     *
     * @return isOnline
     */
    public boolean isOnline() {
        return isOnline;
    }

    /**
     * Ha online van az user beallitjuk true-ra
     *
     * @param online
     */
    public void setOnline(boolean online) {
        isOnline = online;
    }
}
