package hf.chat.model;

import javafx.beans.property.SimpleStringProperty;

import java.io.Serializable;
import java.util.function.Consumer;

/**
 * Created by mate on 2016.11.19..
 */
public class Server extends NetworkConnection {

    private int port;

    /**
     * Constructor
     *
     * @param port
     * @param onReceiveCallback
     */
    public Server(int port, Consumer<Serializable> onReceiveCallback) {
        super(onReceiveCallback);
        this.port = port;
    }

    /**
     * Constuctor
     *
     * @param port
     * @param input
     */
    public Server(int port, SimpleStringProperty input) {
        super(input);
        this.port = port;
    }

    /**
     * @return true, mert szerver
     */
    @Override
    public boolean isServer() {
        return true;
    }

    /**
     *
     * @return null, mert nem fontos a címe
     */
    @Override
    protected String getIP() {
        return null;
    }

    /**
     * Visszaadja a szerver portszámát
     * @return port
     */
    @Override
    protected int getPort() {
        return port;
    }

}
