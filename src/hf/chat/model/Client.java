package hf.chat.model;

import javafx.beans.property.SimpleStringProperty;

import java.io.Serializable;
import java.util.function.Consumer;

/**
 * Created by mate on 2016.11.19..
 */
public class Client extends NetworkConnection {

    private String ip;   // server IP cime
    private int port;

    /**
     * Constructor
     *
     * @param ip
     * @param port
     * @param onReceiveCallback
     */
    public Client(String ip, int port, Consumer<Serializable> onReceiveCallback) {
        super(onReceiveCallback);
        this.port = port;
        this.ip = ip;
    }

    /**
     * Consturctor
     *
     * @param ip
     * @param port
     * @param input
     */
    public Client(String ip, int port, SimpleStringProperty input) {
        super(input);
        this.port = port;
        this.ip = ip;
    }

    /**
     *
     * @return false mert nem szerver
     */
    @Override
    public boolean isServer() {
        return false;
    }

    /**
     *
     * @return ip, ahova csatlakoztunk
     */
    @Override
    protected String getIP() {
        return ip;
    }

    /**
     * visszaadja a portszámot ahova csatlakoztunk
     * @return port
     */
    @Override
    protected int getPort() {
        return port;
    }
}
