package hf.chat.model.filetransfer;

import hf.chat.util.Alert;
import javafx.beans.property.SimpleStringProperty;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;
import java.util.function.Consumer;

/**
 * Created by mate on 2016.12.14..
 */
public abstract class FileConnection {
    public SimpleStringProperty sent = new SimpleStringProperty("false");
    private Consumer<Serializable> onReceiveCallback;
    private ConnectionThread FileConnectionThread = new ConnectionThread();
    private SimpleStringProperty input;
    private Serializable data;
    private File file;

    /**
     * Constructor
     *
     * @param onReceiveCallback
     */
    public FileConnection(Consumer<Serializable> onReceiveCallback) {
        this.onReceiveCallback = onReceiveCallback;
        FileConnectionThread.setDaemon(true);
    }

    public FileConnection(File file) {
        this.file = file;

    }

    public FileConnection(SimpleStringProperty input) {
        this.input = input;
        FileConnectionThread.setDaemon(true);
    }

    /**
     * Elindít egy szálat a csatlakozáshoz.
     *
     * @throws Exception
     */
    public void startFileConnection() throws Exception {
        FileConnectionThread.start();
    }

    /**
     * Bezárja a socketet és a szálat is.
     *
     * @throws Exception
     */
    public void closeFileConnection() throws Exception {
        FileConnectionThread.socket.close();

    }

    /**
     * Bármiylen adat küldhető, amin rajta van a Serializible interface.
     *
     * @param fileToSend
     * @throws Exception
     */
    public void sendFile(File fileToSend) throws Exception {
        int len = 0;
        if (fileToSend.length() > Integer.MAX_VALUE) len = (int) fileToSend.length() / 2;
        else len = (int) fileToSend.length();
        byte[] buffer = new byte[len];

        FileInputStream fis = new FileInputStream(fileToSend);
        BufferedInputStream in = new BufferedInputStream(fis);
        //in.read(buffer,0,buffer.length);
        in.read(buffer);
        //out = socket.getOutputStream();
        FileConnectionThread.out.write(buffer);

        while (true) {
            int data = in.read(buffer);
            System.out.println(data);
            if (data == -1) break;
            FileConnectionThread.out.write(data);
        }
        FileConnectionThread.out.flush();
        Alert succes = new Alert("File sent is succesful");
        in.close();
        FileConnectionThread.out.close();
    }

    /**
     * File-t tudunk másolni
     * // * @param file
     *
     * @throws IOException
     */
    /*public void sendFile(File file) throws IOException {
        new Thread(() -> {
            sent.set("false");
            String propName = "location.properties";
            String path = null;
            Properties locationProp = new Properties();
            File prop = new File(propName);

            if(prop.exists() && !prop.isDirectory()) {
                try {
                    locationProp.load(new FileInputStream(propName));
                    path = locationProp.getProperty("saveDir");
                } catch (IOException e) {
                    System.out.println("Prop not found");
                }

            }
            else path = "/home/mate/";

            File save = new File(path+"/" + file.getName());
            FileOutputStream fout = null;
            int len = 0;
            try {
                System.out.println("File send start...");
                fout = new FileOutputStream(save);
                FileInputStream fin = new FileInputStream(file);
                if(file.length() > Integer.MAX_VALUE) len = (int)file.length()/2;
                else len = (int) file.length();
                byte[] header = new byte[len];
                fin.read(header);
                fout.write(header);
                System.out.println(file.getClass());
                while (true) {
                    int data = fin.read(header);
                    System.out.println(data);
                    if (data == -1) break;
                    fout.write(data);
                }
                fin.close();
                fout.close();
                sent.set("true");
                System.out.println("File sent...");
                System.out.println(save.getAbsolutePath());
            } catch (FileNotFoundException e) {
                System.out.println("File not found..."+ file.getAbsolutePath() +"-->"+save.getAbsolutePath());
            } catch (IOException e1) {
                System.out.println("Error by sending File...");
            }
        }).start();
    }*/
    public String getData() {
        return this.data.toString();
    }

    protected abstract boolean isServer();

    protected abstract String getIP();

    protected abstract int getPort();

    private class ConnectionThread extends Thread {
        private Socket socket;
        private ObjectOutputStream out;

        @Override
        public void run() {
            String propName = "location.properties";
            String path = null;
            Properties locationProp = new Properties();
            File prop = new File(propName);
            int byteread;
            int current;
            byte[] buffer = new byte[4096];
            if (prop.exists() && !prop.isDirectory()) {
                try {
                    locationProp.load(new FileInputStream(propName));
                    path = locationProp.getProperty("saveDir");
                } catch (IOException e) {
                    System.out.println("Prop not found");
                }

            } else path = "/home/mate/";

            File save = new File(path + "/" + file.getName());
            //FileOutputStream fout = null;
            try (ServerSocket server = isServer() ? new ServerSocket(getPort()) : null;
                 Socket socket = isServer() ? server.accept() : new Socket(getIP(), getPort());
                 // ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
                 FileOutputStream fout = new FileOutputStream(save);
                 ObjectInputStream in = new ObjectInputStream(socket.getInputStream())) {

                System.out.println("File Transfer Starting...");
                this.socket = socket;
                this.out = out;
                socket.setTcpNoDelay(true);   // gyorsabb üzenetküldés

                BufferedOutputStream out = new BufferedOutputStream(fout);
                byteread = in.read(buffer, 0, buffer.length);
                current = byteread;

                byteread = in.read(buffer, 0, buffer.length);
                current = byteread;

                do {
                    byteread = in.read(buffer, 0, buffer.length - current);
                    if (byteread >= 0) current += byteread;
                } while (byteread > -1);
                out.write(buffer, 0, current);
                out.flush();

            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }
    } //connectionthread


} //Fileconnection

