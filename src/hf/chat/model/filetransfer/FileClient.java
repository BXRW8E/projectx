package hf.chat.model.filetransfer;


import javafx.beans.property.SimpleStringProperty;

import java.io.*;
import java.net.Socket;
import java.util.Properties;

/**
 * Created by mate on 2016.12.14..
 * Ő az aki fájlt küld
 */

public class FileClient {
    Socket socket = null;
    String fileName = null;   //először elküldjük a nevét
    public SimpleStringProperty sent = new SimpleStringProperty("#false#");


    public FileClient(String host, int port, String file) {
        try {
            sent.set("#FILE#");
            socket = new Socket(host, port);
            sendFile(file);
            sent.set("#ENDFILE#");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendFile(String file) throws IOException {
        DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
        FileInputStream fis = new FileInputStream(file);
        byte[] buffer;
        File filetosend = new File(file);
        String name = filetosend.getName();
        dos.writeUTF(filetosend.getName());  //először a nevét küldjük el
        int size = (int)filetosend.length(); //aztán a hosszát
        dos.writeUTF(Integer.toString(size));
        System.out.println(name);
        System.out.println(size);
        if(size < 4096) buffer = new byte[size];
        else buffer = new byte[4096];

        while (fis.read(buffer) > 0) {
            dos.write(buffer);
        }
        fis.close();
        dos.close();
        socket.close();
    }

    public void close() throws IOException {
        socket.close();
    }
    public static void main(String[] args) {
       FileClient fc = new FileClient("localhost", 1988, "/home/mate/Desktop/getting_started_in_kicad.pdf");
       FileClient fc1 = new FileClient("localhost", 1989, "/home/mate/Desktop/test1");
    }




  /*  public FileClient(int port, String serverIP)  {
        try {
            socket = new Socket(serverIP, port);
        } catch (IOException e) {
            e.printStackTrace();
        }


        InputStream is = null;
        String propName = "location.properties";
        String path = null;
        Properties locationProp = new Properties();
        File prop = new File(propName);

        if(prop.exists() && !prop.isDirectory()) {
            try {
                locationProp.load(new FileInputStream(propName));
                path = locationProp.getProperty("saveDir");
            } catch (IOException e) {
                System.out.println("Prop not found");
            }

        }
        else path = "/home/mate/";

        int len = 4096;
        try {
            BufferedInputStream  fis = new BufferedInputStream(socket.getInputStream());
            DataInputStream din = new DataInputStream(fis);
            fileName = din.readUTF();
            len = Integer.parseInt(din.readUTF());
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Len:" +len);
        System.out.println("file:" + fileName);

        byte[] mybytearray = new byte[len];
        File save = new File(path+"/" + fileName);

        try {
            is = socket.getInputStream();
            FileOutputStream fos = new FileOutputStream(save);
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            int bytesRead = is.read(mybytearray, 0, mybytearray.length);
            bos.write(mybytearray, 0, bytesRead);
            bos.close();
           // socket.close();
            fos.close();
            is.close();
            System.out.println("Received...");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void close() throws IOException {
        socket.close();
    }
    public static void main(String[] args) {
        FileClient client = new FileClient(5556, "127.0.0.1");
    }
    */
} //FileClient