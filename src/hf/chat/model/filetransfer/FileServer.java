package hf.chat.model.filetransfer;


import javafx.beans.property.SimpleStringProperty;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;


/**
 * Created by mate on 2016.12.14..
 * Ő az aki fájlt fogad
 */
public class FileServer extends Thread{
    //public SimpleStringProperty sent = new SimpleStringProperty("#false#");
    public String path;
    public int port;
    public String fileName;
    private ServerSocket serverSocket = null;


    public FileServer(int port) {
        //this.path = path;
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        while (true) {
            try {
                Socket clientSock = serverSocket.accept();
                saveFile(clientSock, getPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void saveFile(Socket clientSock, String path) throws IOException {
        DataInputStream dis = new DataInputStream(clientSock.getInputStream());

        byte[] buffer; //= new byte[4096];

        String fileName = dis.readUTF();
        String absolutePath = path+"/"+fileName;
        FileOutputStream fos = new FileOutputStream(absolutePath);
        System.out.println(absolutePath);
        int filesize = 0; // Send file size in separate msg
        filesize = Integer.parseInt(dis.readUTF());
        System.out.println(filesize);
        if(filesize < 4096) buffer = new byte[filesize];
        else buffer = new byte[4096];

        int read = 0;
        int totalRead = 0;
        int remaining = filesize;
        while((read = dis.read(buffer, 0, Math.min(buffer.length, remaining))) > 0) {
            totalRead += read;
            remaining -= read;
            System.out.println("read " + totalRead + " bytes.");
            fos.write(buffer, 0, read);
        }

        fos.close();
        dis.close();
        // ss.close();
        clientSock.close();
        //ss.close();
    }

    private String getPath() {
        String propName = "location.properties";
        String path = "/home/mate/";
        Properties locationProp = new Properties();
        File prop = new File(propName);

        if(prop.exists() && !prop.isDirectory()) {
            try {
                locationProp.load(new FileInputStream(propName));
                path = locationProp.getProperty("saveDir");
            } catch (IOException e) {
                System.out.println("Prop not found");
            }

        }

        return path;
    }

    public void close() throws IOException {
        serverSocket.close();
    }

    public static void main(String[] args) {
        FileServer fs = new FileServer(1988 );
        fs.start();
        FileServer fs1 = new FileServer(1989);
        fs1.start();
    }





   /* public FileServer(int port){
        this.port = port;
        try {
            this.serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void send(String path){
        File myFile = new File(path);
        fileName = myFile.getName();
        sent.set("#false#");
        try {
            System.out.println("Start transfer...");
            sent.set("#FILE#");
            while (true) {
                Socket sock = serverSocket.accept();
                int len = (int) myFile.length();
                byte[] mybytearray = new byte[len];
                BufferedInputStream bis = new BufferedInputStream(new FileInputStream(myFile));
                bis.read(mybytearray, 0, mybytearray.length);
                OutputStream os = sock.getOutputStream();
                DataOutputStream dout = new DataOutputStream(os);
                dout.writeUTF(fileName);    //először elküldjük a nevét
                dout.writeUTF(Integer.toString(len));
                System.out.println("len:" + len);
                dout.flush();
                os.write(mybytearray, 0, mybytearray.length);
                os.flush();
                sock.close();
                dout.close();
                os.close();
                bis.close();
                sent.set("#true#");
                sent.set("#ENDFILE#");
                System.out.println("Sent...");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() throws IOException {
        serverSocket.close();
    }



    public static void main(String[] args) {
        FileServer server = new FileServer(5556);
        server.send("/home/mate/Desktop/src.zip");
    }
    */
}