package hf.chat.util;

/**
 * Created by mate on 2016.11.29..
 * Ez egy kiegészítő osztály. A csatlakozáshoz tárol adatokat
 */
public class ConnectionSettings {
    private static boolean isServer = true;
    private static String serverIp = "127.0.0.1";
    private static int defaultPort = 5000;
    private static int port;
    private static String nickName;

    public ConnectionSettings() {
    }

    /**
     * @return szerver IP címe
     */
    public static String getServerIp() {
        return serverIp;
    }

    /**
     * Beállítja a szerver IPcímét
     * ez az ahova csatlakozunk.
     *
     * @param serverIp
     */
    public static void setServerIp(String serverIp) {
        ConnectionSettings.serverIp = serverIp;
    }

    /**
     * Felhasználó nevét adja vissza
     *
     * @return nickname
     */
    public static String getNickName() {
        return nickName;
    }

    /**
     * Beállítja a felhasználó nevét
     *
     * @param nickName amit a felhasználó névnek szeretne
     */
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    /**
     * @return true ha szerver
     */
    public static boolean isServer() {
        return isServer;
    }

    /**
     * Beállítja hogy szerver e vagy sem
     *
     * @param isServer
     */
    public void setServer(boolean isServer) {
        this.isServer = isServer;
    }

    /**
     * visszaadja a portszámot amire csatlakozva vagyunk
     * @return port
     */
    public int getPort() {
        return port;
    }

    /**
     * Beállítja a portszámot
     *
     * @param port
     */
    public void setPort(int port) {
        this.port = port;
    }
}
