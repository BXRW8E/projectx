package hf.chat.util;

/**
 * Created by mate on 2016.12.14..
 * Hibaüzenetk megjelenítésére van. Kis hibaüzenet ablakot dob fel, ha kivétel dobódik.
 */
public class Alert {

    public Alert(String msg) {
        javafx.scene.control.Alert alert = new javafx.scene.control.Alert(javafx.scene.control.Alert.AlertType.WARNING);
        alert.initOwner(null);
        alert.setTitle("Error");
        alert.setHeaderText(msg);
        alert.setContentText(msg);
        alert.showAndWait();
    }

    public Alert(String header, String msg) {
        javafx.scene.control.Alert alert = new javafx.scene.control.Alert(javafx.scene.control.Alert.AlertType.WARNING);
        alert.initOwner(null);
        alert.setTitle("Error");
        alert.setHeaderText(header);
        alert.setContentText(msg);
        alert.showAndWait();
    }
}